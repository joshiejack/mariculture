package mariculture.api.core;

import net.minecraft.item.ItemStack;
import net.minecraft.util.Icon;
import mariculture.api.fishery.IBiomeType;

public class MaricultureHandlers {
	public static IBiomeType biomeType;
	public static ISmelterHandler smelter;
	public static IFreezerHandler freezer;
	public static IMirrorHandler mirror;
	public static IUpgradeHandler upgrades;
	public static IGasTurbine turbine;
	public static IModules modules;
}
