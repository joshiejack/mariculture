package mariculture.core.gui;

import java.util.List;

import net.minecraft.item.ItemStack;

public interface IToolTip {
	public void addItemToolTip(ItemStack stack, List list);
}
