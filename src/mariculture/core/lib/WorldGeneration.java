package mariculture.core.lib;

public class WorldGeneration {
	public static boolean CORAL_ENABLED;
	public static int CORAL_CHANCE;
	public static int CORAL_DEPTH;
	public static int KELP_CHANCE;
	public static int KELP_DEPTH;
	public static int KELP_HEIGHT;
	public static boolean KELP_PATCH_ENABLED;
	public static int KELP_PATCH_DENSITY;
	public static boolean KELP_FOREST_ENABLED;
	public static int KELP_FOREST_DENSITY;
	public static int KELP_CHEST_CHANCE;
	public static boolean DEEP_OCEAN;
	public static boolean WATER_RAVINES;
	public static boolean NO_MINESHAFTS;
	public static boolean WATER_CAVES;
	public static int RAVINE_CHANCE;
}
