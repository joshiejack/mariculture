package mariculture.core.lib;

public class BaitMeta {
	public static final int COUNT = 4;
	
	public static final int WORM = 0;
	public static final int ANT = 1;
	public static final int MAGGOT = 2;
	public static final int HOPPER = 3;
}
