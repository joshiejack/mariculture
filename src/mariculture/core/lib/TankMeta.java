package mariculture.core.lib;

public class TankMeta {
	public static final int COUNT = 2;
	public static final int TANK = 0;
	public static final int FISH = 1;
}
