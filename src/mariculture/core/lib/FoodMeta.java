package mariculture.core.lib;

public class FoodMeta {
	public static final int COUNT = 3;

	public static final int FISH_FINGER = 0;
	public static final int CALAMARI = 1;
	public static final int SMOKED_SALMON = 2;
}
