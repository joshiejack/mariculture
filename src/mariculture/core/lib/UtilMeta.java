package mariculture.core.lib;

public class UtilMeta {
	public static final int COUNT = 12;

	public static final int INCUBATOR_BASE = 0;
	public static final int INCUBATOR_TOP = 1;
	public static final int AUTOFISHER = 2;
	public static final int LIQUIFIER = 3;
	public static final int SETTLER = 4;
	public static final int PURIFIER = 5;
	public static final int BOOKSHELF = 6;
	public static final int SAWMILL = 7;
	public static final int SLUICE = 8;
	public static final int SPONGE = 9;
	public static final int DICTIONARY = 10;
	public static final int FISH_SORTER = 11;
}
