package mariculture.core.lib;

public class SingleMeta {
	public static final int COUNT = 8;

	public static final int AIR_PUMP = 0;
	public static final int FISH_FEEDER = 1;
	public static final int NET = 2;
	public static final int TURBINE_WATER = 3;
	public static final int FLUDD_STAND = 4;
	public static final int TURBINE_GAS = 5;
	public static final int GEYSER = 6;
	public static final int TURBINE_HAND = 7;
}
