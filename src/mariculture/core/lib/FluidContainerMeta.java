package mariculture.core.lib;

public class FluidContainerMeta {
	public static final int COUNT = 18;

	public static final int BOTTLE_VOID = 0;
	public static final int BOTTLE_FISH_OIL = 1;
	public static final int BOTTLE_GAS = 2;
	public static final int BOTTLE_IRON = 3;
	public static final int BOTTLE_GOLD = 4;
	public static final int BOTTLE_COPPER = 5;
	public static final int BOTTLE_TIN = 6;
	public static final int BOTTLE_SILVER = 7;
	public static final int BOTTLE_LEAD = 8;
	public static final int BOTTLE_BRONZE = 9;
	public static final int BOTTLE_STEEL = 10;
	public static final int BOTTLE_ALUMINUM = 11;
	public static final int BOTTLE_TITANIUM = 12;
	public static final int BOTTLE_MAGNESIUM = 13;
	public static final int BOTTLE_NICKEL = 14;
	public static final int BOTTLE_GLASS = 15;
	public static final int BOTTLE_FISH_FOOD = 16;
	public static final int BOTTLE_RUTILE = 17;
}
