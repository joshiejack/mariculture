package mariculture.core.lib;

public class WoodMeta {
	public static final int COUNT = 3;
	public static final int POLISHED_PLANK = 0;
	public static final int POLISHED_LOG = 1;
	public static final int BASE_WOOD = 2;
}
