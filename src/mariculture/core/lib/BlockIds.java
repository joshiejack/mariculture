package mariculture.core.lib;

public class BlockIds {
	public static int ores;
	public static int util;
	public static int oyster;
	public static int sift;
	public static int doubleBlocks;
	public static int singleBlocks;
	public static int limestoneStairs;
	public static int limestoneBrickStairs;
	public static int limestoneSmoothStairs;
	public static int limestoneChiseledStairs;
	public static int limestoneSlabs;
	public static int limestoneDoubleSlabs;
	public static int lampsOn;
	public static int lampsOff;
	public static int pearlBrick;
	public static int coral;
	public static int glassBlocks;
	public static int airBlocks;
	public static int woodBlocks;
	public static int tankBlocks;
	
	public static int customFlooring;
	public static int customBlock;
	public static int customStairs;
	public static int customSlabs;
	public static int customSlabsDouble;
	public static int customFence;
	public static int customGate;
	public static int customWall;
	public static int customLight;
	public static int customRFBlock;

	/* liquids */
	public static int highPressureWater;
}
