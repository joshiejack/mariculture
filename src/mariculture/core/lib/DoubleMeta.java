package mariculture.core.lib;

public class DoubleMeta {
	public static final int COUNT = 3;
	
	public static final int AIR_COMPRESSOR = 0;
	public static final int AIR_COMPRESSOR_POWER = 1;
	public static final int PRESSURE_VESSEL = 2;
}
