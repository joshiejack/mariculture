package mariculture.core.lib;

public class MaterialsMeta {
	public static final int COUNT = 27;

	public static final int INGOT_ALUMINUM = 0;
	public static final int INGOT_MAGNESIUM = 1;
	public static final int INGOT_TITANIUM = 2;
	public static final int UNUSED9 = 3;
	public static final int INGOT_COPPER = 4;
	public static final int UNUSED0 = 5;
	public static final int UNUSED5 = 6;
	public static final int UNUSED6 = 7;
	public static final int UNUSED7 = 8;
	public static final int UNUSED8 = 9;
	public static final int UNUSED1 = 10;
	public static final int UNUSED2 = 11;
	public static final int UNUSED3 = 12;
	public static final int UNUSED4 = 13;
	public static final int FISH_MEAL = 14;
	public static final int DYE_YELLOW = 15;
	public static final int DYE_RED = 16;
	public static final int DYE_BROWN = 17;
	public static final int DROP_WATER = 18;
	public static final int DROP_NETHER = 19;
	public static final int DROP_ENDER = 20;
	public static final int DROP_ATTACK = 21;
	public static final int DROP_AQUA = 22;
	public static final int DROP_POISON = 23;
	public static final int DROP_MAGIC = 24;
	public static final int DROP_ELECTRIC = 25;
	public static final int DROP_HEALTH = 26;
}
