package mariculture.core.lib;

public class Dye {
	public static final int LIGHT_BLUE = 12;
	public static final int MAGENTA = 13;
	public static final int ORANGE = 14;
	public static final int PINK = 9;
	public static final int PURPLE = 5;
	public static final int RED = 1;
	public static final int BONE = 15;
	public static final int YELLOW = 11;
	public static final int LAPIS = 4;
	public static final int INK = 0;
}
