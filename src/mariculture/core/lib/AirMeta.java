package mariculture.core.lib;

public class AirMeta {
	public static final int COUNT = 3;
	public static final int NATURAL_GAS = 0;
	public static final int FAKE_AIR = 1;
	public static final int DEMO = 2;
}
