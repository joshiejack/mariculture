package mariculture.core.lib;

public class GuiIds {
	public static final int OYSTER = 0;
	public static final int MIRROR = 1;
	public static final int AUTOFISHER = 2;
	public static final int SETTLER = 3;
	public static final int LIQUIFIER = 4;
	public static final int SIFT = 5;
	public static final int FEEDER = 6;
	public static final int INCUBATOR = 7;
	public static final int BOOKSHELF = 8;
	public static final int SAWMILL = 9;
	public static final int TURBINE = 10;
	public static final int FLUDD_BLOCK = 11;
	public static final int PRESSURE_VESSEL = 12;
	public static final int DICTIONARY = 13;
	public static final int SLUICE = 14;
	public static final int TURBINE_GAS = 15;
	public static final int FILTER = 16;
	public static final int TILE_GUI = 17;
}
