package mariculture.diving;

import mariculture.core.Core;
import mariculture.core.helpers.PlayerHelper;
import mariculture.core.lib.ArmorSlot;
import mariculture.core.lib.Modules;
import net.minecraft.block.material.Material;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemTool;
import net.minecraftforge.common.ForgeHooks;
import net.minecraftforge.event.ForgeSubscribe;
import net.minecraftforge.event.entity.living.LivingEvent.LivingUpdateEvent;
import net.minecraftforge.event.entity.player.PlayerEvent.BreakSpeed;

public class ArmorEventHandler {
	@ForgeSubscribe
	public void onLivingUpdate(LivingUpdateEvent event) {
		if (event.entity instanceof EntityPlayer) {
			EntityPlayer player = (EntityPlayer) event.entity;
			if (!player.worldObj.isRemote) {
				ScubaTank.init(player);
				ScubaMask.damage(player);
			} else {
				DivingBoots.init(player);
				DivingHelmet.init(player);
				ScubaFin.init(player);
				ScubaMask.init(player);
			}
		}
	}

	@ForgeSubscribe
	public void onBreaking(BreakSpeed event) {
		EntityPlayer player = event.entityPlayer;
		boolean isValid = false;
		
		if(ForgeHooks.canHarvestBlock(event.block, player, event.metadata)) {
			if (player.isInsideOfMaterial(Material.water)) {
				// Scuba Suit
				if (PlayerHelper.hasArmor(player, ArmorSlot.LEG, Diving.scubaSuit)) {
					event.newSpeed = event.originalSpeed * 4;
					
					if (event.block.blockID == Core.oysterBlock.blockID) {
						event.newSpeed = event.originalSpeed * 128;
					}
				}
				
				if(PlayerHelper.hasArmor(player, ArmorSlot.FEET, Diving.swimfin) && !player.onGround) {
					event.newSpeed*=5;
				}
	
				// Diving Pants
				if (PlayerHelper.hasArmor(player, ArmorSlot.LEG, Diving.divingPants)) {
					event.newSpeed = event.originalSpeed * 2;
					if (event.block.blockID == Core.oysterBlock.blockID) {
						event.newSpeed = event.originalSpeed * 64;
					}
				}
			}
		}
	}
}
